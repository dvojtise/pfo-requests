<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues
and verify the issue you're about to submit isn't a duplicate.
--->


### Problem to solve

<!-- What problem do we solve? Try to define the who/what/why of the opportunity as a user story. For example, "As a (who), I want (what), so I can (why/value)." -->


### Proposal

<!-- If you know some details about possible solutions,-->
<!-- Use this section to explain the feature and how it will work. It can be helpful to add technical details, design proposals, and links to related epics or issues. -->



### Links / references




<!-- Label reminders
Use the following resources to find the appropriate labels:
- Use only one tier label choosing the lowest tier this is intended for
- https://gitlab.com/gitlab-org/gitlab/-/labels
- https://about.gitlab.com/handbook/product/categories/features/
-->

/label ~group:: ~section:: ~Category:
/label ~"GitLab" ~"GitLab-int" ~"Sonarqube" ~"CI"
/label ~"type::feature" ~"feature::addition" ~documentation

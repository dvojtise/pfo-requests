# PFO Requests


The goal of the `PFO Request` is to collaboratively collect requests related to the INRIA's `Platformes Outils` related to development and experimentations.

This will:
- help determining if an issue or request is applicable to only a few people or if many user are impacted.
- help communicating progress on the issue by referencing meetings or documents.

It works as a lightweight public issue tracking system based on gitlab issues.


Example platforms:
- gitlab
- CI
    - shared_runner
    - cloudstack
- sonarqube
- mattemost
- sharelatex


:warning: **IMPORTANT**

For now, this project applies only to the platforms related to development and experimentations. It does NOT apply to administrative service platforms.  (If this workflow is efficient, maybe CUMIs will instanciate a similar tracking system )

